TARGET=dagstuhl-21243-report
TEMPFILES=$(foreach s, tex toc xml aux bbl blg log pdf synctex.gz zip, $(TARGET).$s)

EXTRA = -synctex=1
LATEX = pdflatex ${EXTRA} $< && (ls *.aux | xargs -n 1 bibtex) || pdflatex ${EXTRA} $< || pdflatex ${EXTRA} $< || pdflatex ${EXTRA} $<


%.tex: %-clean.html
	pandoc --template=template.latex --metadata-file=metadata.yaml -V graphics -V title="Dagstuhl Seminar 21243 Report" --toc --include-before-body "include.tex"   -s $< -o $@


%.pdf: %.tex
	$(LATEX)

%.xml: %.html
	-tidy -q -asxml -indent --numeric-entities yes $< >$@

%-clean.html: %.xml
	saxon -s:$< -o:$@ -xsl:report.xsl


$(TARGET).zip: $(TARGET).tex $(TARGET).pdf images
	zip -9 -r $@ $^


all: $(TARGET).pdf

tex: $(TARGET).tex

xml: $(TARGET).xml

dist: $(TARGET).zip


clean:
	rm -f *.aux *.bbl *.blg *.log *.pdf *.synctex.gz test.tex *~ $(TEMPFILES)


