<?xml version="1.0"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:h="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" indent="yes"/>
  
  

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  
  <xsl:template match="h:a">
    <xsl:element name="a" namespace="http://www.w3.org/1999/xhtml">
      <xsl:attribute name="href"><xsl:value-of select="normalize-space(.)" /></xsl:attribute>
      <xsl:value-of select="normalize-space(.)" />
    </xsl:element>
  </xsl:template>
  

    
    
</xsl:stylesheet>
